$(document).ready(function() {
    $("#employee_rate, #hours_worked").keyup(function () {
        rate = parseFloat($("#employee_rate").val());
        hours_worked = parseFloat($("#hours_worked").val());
        amount_owed = rate * hours_worked;
        $("#amount_owed").val(amount_owed);
    })
})